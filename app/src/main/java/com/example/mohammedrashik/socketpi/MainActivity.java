package com.example.mohammedrashik.socketpi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {
Socket socket;
EditText message,ip,sockets;
Button send,connect;
String data;
    DataOutputStream dout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        message = (EditText)findViewById(R.id.message);
        ip = (EditText)findViewById(R.id.ipaddress);
        sockets = (EditText)findViewById(R.id.socket);
        send = (Button)findViewById(R.id.send);
        connect = (Button)findViewById(R.id.connect);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try
                {
                    new Thread(new Runnable()  {
                        @Override
                        public void run() {
                            try {
                                socket  = new Socket(ip.getText().toString(),Integer.parseInt(sockets.getText().toString())) ;
                                Log.d("STATUS",socket.getInetAddress().toString());
                                if(socket.isConnected())
                                {
                                    Toast.makeText(MainActivity.this, "CONNECTED TO"+socket.getInetAddress().toString(), Toast.LENGTH_SHORT).show();


                                    Log.d("CONNECTED",socket.isConnected()+"");
                                }else
                                {
                                    Toast.makeText(MainActivity.this, "NOT CONNECTED ", Toast.LENGTH_SHORT).show();
                                    Log.d("NOT CONNECTED",socket.isConnected()+"");
                                }


                            }catch (Exception e)
                            {

                                Log.d("CONNECTION PROBLEM ",e.toString());
                            }

                        }
                    }).start();

                }catch (Exception e)
                {
                    Log.d("EXCEPTION ",e.toString());
                }
            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if(socket.isConnected())
                    {

                    }else
                    {


                       return;
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            data = message.getText().toString();
                            try {
                                dout=new DataOutputStream(socket.getOutputStream());
                                dout.writeUTF(data+"");
                                dout.flush();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();

                }catch (Exception e)
                {
                    Log.d("EXCEPTION",e.toString());
                }

            }
        });
    }
}
